document.querySelector('.menu').addEventListener('click', () => {
  //here each has a common class target
  //this returns an array like object called node list
  // in order to loop through node list -- we can use foreach

  document.querySelectorAll('.target').forEach((item) => {
    item.classList.toggle('change');
  });
});

/* here we also close the navbar on item click in teh navbar */
document.querySelectorAll('.wrapper').forEach((item) => {
  item.addEventListener('click', () => {
    //same code as above -- here click on navbar item open and close
    document.querySelectorAll('.target').forEach((item) => {
      item.classList.remove('change');
    });
  });
});

const videos = document.querySelectorAll('.video');

// note we are using mousover and mouse out to start and stop
videos.forEach((video) => {
  video.addEventListener('mouseover', () => {
    video.play();
  });
  video.addEventListener('mouseout', () => {
    video.pause();
  });
});
